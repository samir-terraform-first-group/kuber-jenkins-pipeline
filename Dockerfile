FROM centos:7
RUN yum install httpd -y
RUN echo "This is testing for kubernetes jenkins pipeline" > /var/www/html/index.html
EXPOSE 80
CMD ["httpd","-D","FOREGROUND"]

